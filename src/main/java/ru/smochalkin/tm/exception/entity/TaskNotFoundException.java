package ru.smochalkin.tm.exception.entity;

import ru.smochalkin.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
