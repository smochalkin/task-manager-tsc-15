package ru.smochalkin.tm.controller;

import ru.smochalkin.tm.api.controller.IProjectTaskController;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.exception.entity.TaskNotFoundException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskByProjectId(){
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        System.out.print("Enter task id: ");
        String taskId = TerminalUtil.nextLine();
        if (!projectTaskService.isTaskId(taskId)) {
            throw new TaskNotFoundException();
        }
        projectTaskService.bindTaskByProjectId(projectId, taskId);
    }

    @Override
    public void unbindTaskByProjectId(){
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        System.out.print("Enter task id: ");
        String taskId = TerminalUtil.nextLine();
        if (!projectTaskService.isTaskId(taskId)) {
            throw new TaskNotFoundException();
        }
        projectTaskService.unbindTaskByProjectId(projectId, taskId);
    }

    @Override
    public void showTasksByProjectId(){
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        System.out.println("[TASK LIST BY PROJECT ID = "+ projectId + "]");
        List<Task> tasks = projectTaskService.findTasksByProjectId(projectId);
        for (Task task : tasks) {
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById(){
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        projectTaskService.removeProjectById(projectId);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName(){
        System.out.print("Enter project name: ");
        String projectName = TerminalUtil.nextLine();
        if (!projectTaskService.isProjectName(projectName)) {
            throw new ProjectNotFoundException();
        }
        projectTaskService.removeProjectByName(projectName);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex(){
        System.out.print("Enter project index: ");
        Integer index = TerminalUtil.nextInt();
        if (!projectTaskService.isProjectIndex(index)) {
            throw new ProjectNotFoundException();
        }
        projectTaskService.removeProjectByIndex(index);
        System.out.println("[OK]");
    }

}
